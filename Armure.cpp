#include "Armure.h"
#include "Objet.h"
#include <iostream>

Armure::Armure():Objet()
{
    std::cout<< "Armure creee" <<std::endl;
}

Armure::~Armure()
{
    std::cout<<"Armure detruite"<<std::endl;
}
