//
//  Split.h
//  decoupage_texte
//
//  Created by Amaury GIBERT on 11/05/2020.
//  Copyright © 2020 Amaury GIBERT. All rights reserved.
//

#ifndef Split_h
#define Split_h

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

template <typename T>
class Split {
public:
    Split();
    
    std::vector<std::string> tabsplit(std::string chaine, char delimiteur);
private:
    std::string mot;
    std::vector<std::string> elements;
};

template <typename T>
Split<T>::Split(){
    
}

template <typename T>
std::vector<std::string>Split<T>::tabsplit(std::string chaine, char delimiteur){
    std::istringstream iss(chaine );
    elements.clear();
    while (std::getline(iss, this->mot, delimiteur)) {
        elements.push_back(this->mot);
    }
    return elements;
}


#endif /* Split_h */
