//
//  main.cpp
//  Jeu_de_Role
//
//  Created by Amaury GIBERT on 06/05/2020.
//  Copyright © 2020 Amaury GIBERT. All rights reserved.
//

#include <iostream>
#include <string>

#include "Histoire.hpp"
#include "Utilisateur.hpp"
#include "Objet.h"
#include "Arme.h"
#include "Armure.h"

int main(int argc, const char * argv[]) {
    Utilisateur* utilisateur = new Utilisateur();
    utilisateur->creation_personnage("Toto");
    
//    Arme* Stock_Armes =  new Arme("Stock");
//    Stock_Armes->mesArmes();
    
    Histoire* histoire = new Histoire(utilisateur);
    
//    Stock_Armes->affiche();
    
    utilisateur->getPersonnage(0)->getEquipement()->equip("Hache");
    
    int j = 0;
    char c;
    srand(time(NULL));
    std::string adresse = "/Users/amaurygibert/Desktop/projet_cpp/Projet_final/jdr1/jdr1/text_jdr/";
    char suite = '1';
    bool isGood = false;
    const char* adress = adresse.c_str();
    std::vector<std::string> num_suivant;
    std::cout << "********************************************************************************************" << std::endl;
    std::cout << "*\t\t\t\t\tVous allez commencer une histoire merveilleuse !\t\t\t\t\t   *" << std::endl;
    std::cout << "*\t\t\t\t\t\t\t\tEtes-vous prêt à en découdre ?\t\t\t\t\t\t\t   *" << std::endl;
    
    while (utilisateur->getPersonnage(0)->getPv() > 0)
    {
        adresse = "/Users/amaurygibert/Desktop/projet_cpp/Projet_final/jdr1/jdr1/text_jdr/";
        adresse += suite;
        adresse += ".txt";
        
        j = histoire->lire_fichier(adress);
        std::cout << "********************************************************************************************" << std::endl;
        num_suivant = histoire->analyse_fichier();
        histoire->affiche_texte();
        histoire->actions();
        
        std::cout << std::endl;
        
        while (!isGood)
        {
            std::cout << "Tapez ";
            for(auto i = 0 ; i < num_suivant.size() ; i++)
            {
                for(auto j = 0 ; j < num_suivant[i].size() ; j++ )
                {
                    std::cout << num_suivant[i][j];
                }
                if (i < num_suivant.size()-1)
                    std::cout << " ou ";
            }
            std::cout << " : " << std::endl;
            
            suite = getchar();
            while ((c = getchar()) != '\n' && c != EOF);
            for(auto i = 0 ; i < num_suivant.size() ; i++)
            {
                if (num_suivant[i][0] == suite)
                    isGood = true;
            }
        }
        isGood = false;
    }
    
    delete histoire;
    
    return 0;
}
