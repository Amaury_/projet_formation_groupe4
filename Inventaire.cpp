// Auteur : NABLi Hatem
//Date : 06/05/2020
#include "Inventaire.h"
#include "Objet.h"
#include <algorithm>
#include <iostream>
#include <string>
/*
------------------------
    Constructeur
------------------------
*/
Inventaire::Inventaire(int taille)
{
    this->setTaille(taille);
    this->nbObjet = 0;
    std::cout<<"creation de l'inventaire"<<std::endl;
    //ctor
}

Inventaire::~Inventaire()
{
    /*for(auto i = 0 ; i < this->objets.size() ; i++)
    {
        delete this->objets[i];
    }*/
    std::cout<<"Destruction de l'inventaire : "<<std::endl;
}
/*
-----------------------------
    modificateur de taille
-----------------------------
*/
void Inventaire::setTaille(int taille)
{
    this->taille = taille;
}
/*
---------------------------------
    put object in inventaire
---------------------------------
*/
void Inventaire::putInInventaire(std::string objet)
{
    if(this->nbObjet < this->taille)
    {
        this->objets.push_back(objet);
        std::cout<<"ajout de l'objet "<<objet<<" dans l'inventaire."<<std::endl;
        this->nbObjet++;
    }
    else
    {
        std::cout<<" Inventaire plein !! "<<std::endl;
    }
}
/*
------------------------------------
    Delete object from Inventaire
-------------------------------------
*/

bool Inventaire::getOutOfInventaire(std::string objet)
{
    std::vector<std::string>::iterator it =std::find(this->objets.begin(),this->objets.end(),objet);
    std::vector<std::string>::difference_type index = std::distance(this->objets.begin(), it);
    if(it!=objets.end())
    {
        std::cout<<"Oui j'ai l'objet"<<std::endl;
        this->objets.erase(it);
        this->nbObjet--;
        std::cout<<"supression d'un objet"<<std::endl;
        return true ;
    }
    else
    {
        return false ;
    }

    std::cout<<"supression d'un objet"<<std::endl;
}
/*
-----------------------------------
    Display all Items
-----------------------------------
*/
void Inventaire::DisplayAllItems()
{
    std::cout << "Contenu de l'inventaire :" << std::endl;
    for(int i = 0 ; i < this->objets.size(); i++)
    {
        std::cout<<objets[i]<<std::endl;
    }
}

