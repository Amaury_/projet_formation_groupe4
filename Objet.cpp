// Auteur : NABLi Hatem
//Date : 06/05/2020
#include "Objet.h"
#include <iostream>
#include <string>

Objet::Objet()
{
    //this->setName(name);
    std::cout<<" l'objet : "<<this->getName()<<" a ete cree"<<std::endl;

}

Objet::~Objet()
{
    std::cout<<"Destruction de l'objet : "<<this->getName()<<std::endl;
}

/*
-----------------------------------------
 modificateur du nom
-----------------------------------------
*/
void Objet::setName(std::string name)
{
    this->name = name;
}

